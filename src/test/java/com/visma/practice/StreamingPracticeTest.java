package com.visma.practice;

import com.visma.practice.domene.Lake;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * @author Leo-Andreas Ervik
 */
public class StreamingPracticeTest {

	@Test
	public void shouldFindLakesStartingWithC() throws Exception {
		List<Lake> innsjoerSomBegynnerPaaC = StreamingPractice.startsWithC();
		assertEquals(1, innsjoerSomBegynnerPaaC.size());
	}

}